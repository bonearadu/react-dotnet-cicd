import { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

const App = () => {
  const [forecast, setForecast] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch('http://localhost:5000/WeatherForecast')
      .then(response => response.json())
      .then(f => {
        setForecast(f);
        setLoading(false);
      })
      .catch(err => alert(err))
  }, []);

  if (!loading) {
    console.log(forecast)
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
